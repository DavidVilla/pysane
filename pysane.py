#! /usr/bin/python
# -*- coding:utf-8 -*-

import sys, os, time
import optparse
import subprocess
import logging
logging.basicConfig(format='%(levelname)s:  %(message)s', level=logging.DEBUG)

import sane

class Scanner:
    def __init__(self, usbid, manufacturer, model, kind):
        self.usbid = usbid
        self.manufacturer = manufacturer
        self.model = model
        self.kind = kind
        self.device = None

    def open(self, **kargs):
        self.device = sane.open(self.usbid)

        for key,val in kargs.items():
            setattr(self.device, key, val)

    def get_parameters(self):
        return str.join(', ', [str(x) for x in self.device.get_parameters()])

    def scan_pages(self):
        files = []
        for i, img in enumerate(self.device.multi_scan()):
            fname = os.path.join(dirname, 'page-%02d.pdf' % (i+1))
            files.append(fname)

            logging.info("Saving page on '%s'", fname)
            img.save(fname)

            try:
                if raw_input('Another page? (Y/n): ').lower().startswith('n'):
                    break
            except KeyboardInterrupt:
                print '\nC-c pressed'
                break

        logging.info("Scanned pages: %s", (i+1))
        return files

    def close(self):
        self.device.close()

    def __str__(self):
        return "<Scanner {0} {1}>".format(
            self.manufacturer, self.model)


def get_scanners():
    sane.init()
    detected = [Scanner(*x) for x in sane.get_devices()]

    logging.info('Available devices:')
    for i in  detected:
        logging.info("- %s", i)

    return detected


parser = optparse.OptionParser()
options, args = parser.parse_args()

output = args[0] if args else 'output.pdf'
dirname = '/tmp/pysane-%s' % os.getpid()
os.mkdir(dirname)

try:
    scanner = get_scanners()[0]
    scanner.open(mode='color', resolution=300)
    subprocess.Popen(["nautilus", dirname])
    files = scanner.scan_pages()
except IndexError:
    logging.error("No scanners detected")
    sys.exit(1)



if len(files) == 1:
    os.system("mv '%s' '%s'" % (files[0], output))
else:
    logging.info("joining pages...")
    os.system("pdfjoin -q --fitpaper true --outfile {0} {1}".format(
            output, str.join(' ', files)))

logging.info("Final PDF saved in '%s'", os.path.abspath(output))
